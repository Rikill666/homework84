const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const TaskScheme = new Scheme({
    user:{
        type: Scheme.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required: true
    },
    status:{
        type: String,
        enum: ['new', 'in_progress', 'complete'],
        default:'new',
        required: true
    },
});

const Task= mongoose.model("Task", TaskScheme);
module.exports = Task;