const express = require('express');

const User = require('../models/User');
const Task = require('../models/Task');
const auth = require('../middleware/auth');
const router = express.Router();

router.post('/', auth, async (req, res) => {
    const user = req.user;
    try {
        const task = new Task({user:user._id, title: req.body.title, description: req.body.description});
        await task.save();
        return res.send(task);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.get('/', auth, async (req, res) => {
    const user = req.user;
    try{
        const tasks = await Task.find({user: user._id});
        return res.send(tasks);
    }
    catch (error) {
        return res.status(400).send(error);
    }
});

router.put('/:id', auth, async (req, res) => {
    const user = req.user;
    const taskId = req.params.id;
    try{
        const task = await Task.findOne({_id: taskId});
        if(!task || task.user.toString() !== user._id.toString()){
            return res.status(404).send({message:"Task not found"});
        }
        try{
            const updateTask = new Task({user:user._id, title: req.body.title, description: req.body.description, status: req.body.status});
            await updateTask.save();
            return res.send(updateTask);
        }
        catch(error){
            return res.status(400).send(error);
        }
    }
    catch (error) {
        return res.status(400).send(error);
    }
});

router.delete('/:id', auth, async (req, res) => {
    const user = req.user;
    const taskId = req.params.id;
    try{
        const task = await Task.findOne({_id: taskId});
        if(!task || task.user.toString() !== user._id.toString()){
            return res.status(404).send({message:"Task not found"});
        }
        try{
            await Task.deleteOne({_id: taskId});
            res.send('ok');
        }
        catch(error){
            return res.status(400).send(error);
        }
    }
    catch (error) {
        return res.status(400).send(error);
    }
});
module.exports = router;